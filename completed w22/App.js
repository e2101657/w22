import React from 'react';
import './App.css';

const ComponentName = (props) => {
return (
   <div>
  <p>Hello {props.country}!</p>
  </div>
);
}
function App() {
  return (
    <div className="Container">
      <h1>W22 Hello country example</h1>
    <ComponentName country="Finland" />
  </div>
);
}
export default App;
